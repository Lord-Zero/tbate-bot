from __future__ import annotations
import asyncio
from src.Bot import TBATE_Bot


try:
    import uvloop
except ImportError:
    print("Package uvloop is not installed. Using python's default asyncio loop.")
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


if __name__ == '__main__':
    BOT = TBATE_Bot()
    print("Starting TBATE Bot...")
    BOT.run()
else:
    raise ImportWarning("Launcher was attempted to be imported.")
