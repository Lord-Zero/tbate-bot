from __future__ import annotations

from os import environ
from typing import TYPE_CHECKING

import discord
from discord.ext import commands


class TBATE_Bot(commands.Bot):
    """
    Driver-Class for the project. Must be instantiated in order to run the bot.
    """

    __slots__ = (
        'VERSION',
        'MONGO_URI',
        'TOKEN',
        'FFMPEG_EXECUTABLE',
        'OWNERS',
    )

    def __init__(self) -> None:
        self.VERSION: str = '0.0.0'
        self.MONGO_URI: str = environ['MONGO_URI']
        self.TOKEN: str = environ['TOKEN']
        self.FFMPEG_EXECUTABLE: str = environ.get('FFMPEG_EXECUTABLE', 'ffmpeg')
        self.OWNERS: list[int] = [654611403478401055]

        bot_intents = discord.Intents.default()
        bot_intents.members = True
        bot_intents.message_content = True

        super().__init__(
            command_prefix=self._PREFIX_,
            case_insensitive=True,
            owner_ids=self.OWNERS,
            intents=bot_intents,
        )

    def run(self) -> None:
        super().run(self.TOKEN)

    async def _PREFIX_(self, bot: TBATE_Bot, message: discord.Message) -> str:
        return "!"
